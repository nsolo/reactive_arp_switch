from scapy.all import ARP, Ether, sendp, wrpcap
import random
import string
import sys

def randomMAC():
    mac = [ 0x00,
            random.randint(0x00, 0x7f),
            random.randint(0x00, 0x7f),
            random.randint(0x00, 0x7f),
            random.randint(0x00, 0xff),
            random.randint(0x00, 0xff) ]
    return ':'.join(map(lambda x: "%02x" % x, mac))

def randomIP():
    not_valid = [10,127,169,172,192]

    first = random.randrange(1,256)
    while first in not_valid:
        first = random.randrange(1,256)

    ip = ".".join([str(first),str(random.randrange(1,256)),
    str(random.randrange(1,256)),str(random.randrange(1,256))])
    return ip
    
def generate_packet(senderMAC, ip, randomPayloadLen):
    p = ARP()
    p.hwsrc = senderMAC
    p.hwdst = "99:99:99:99:99:99"
    p.psrc = ip
    p.pdst = ip

    p = Ether() / p
    p.src = senderMAC
    p.dst = "99:99:99:99:99:99"

    payload = ''.join(random.choice(string.ascii_letters + string.digits) for x in range(randomPayloadLen))
    p = p / payload

    return p

def generate_packet_list(length):
    packets = []
    for i in xrange(length):
        random_mac = randomMAC()
        random_ip = randomIP()
        generated_packet = generate_packet(random_mac, random_ip, 100)
        packets.append(generated_packet)
    return packets
    
def parse_conf_file(conf_file):
    data = open(conf_file, 'r').readlines()
    return [int(x) for x in data]

if __name__ == "__main__":
    if sys.argv[1].isdigit():
        lengths = [int(sys.argv[1])]
    else:
        conf_file = sys.argv[1]
        lengths = parse_conf_file(conf_file)
    for length in lengths:
        packets = generate_packet_list(length)
        wrpcap("./output/output_gen_%d.pcap" % length, packets)