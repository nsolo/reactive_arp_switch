"""Reactive ARP Query learning switch logic using OpenFlow Protocol v1.3."""
from collections import defaultdict
from ryu.ofproto import ether
from ryu.base.app_manager import RyuApp
from ryu.controller import ofp_event
from ryu.controller.handler import set_ev_cls, CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.lib.packet import ipv4, ethernet, udp
from ryu.lib.packet.arp import arp
from ryu.ofproto.ofproto_v1_2 import OFPG_ANY, OFP_VERSION
from ryu.ofproto.ofproto_v1_3 import OFPP_LOCAL
from ryu.lib.packet import packet
import json
from threading import Thread, Lock
from ryu.ofproto.ofproto_v1_3_parser import OFPSetConfig

TABLE_ETH_SRC = 0
TABLE_ETH_DST = 1
PRIORITY = 123
DHCP_PORT = 67

SWITCH_TABLES = [TABLE_ETH_SRC, TABLE_ETH_DST]

REACTIVE_ARP_FAKE_MAC = "00:11:22:33:44:55"
REACTIVE_ARP_SRC_IP = "10.99.99.99"
BROADCAST_MAC = "ff:ff:ff:ff:ff:ff"


class L2Switch(RyuApp):
    OFP_VERSIONS = [OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(L2Switch, self).__init__(*args, **kwargs)
        self.mac_to_port = defaultdict(lambda: dict())
        self.ip_to_mac = {}
        self.packets_to_verify = defaultdict(list)
        self.limited_ports = {}
        self.number_of_flows = defaultdict(int)
        self.number_of_packet_ins = 0
        self.all_packets = 0
        self.lock = Lock()
        self.dropped_macs = []
        self.enabled_meters = {}
        
        # Configuration
        self.enable_reactive_arp = True
        self.should_install_metering = True
        self.force_flood = False
        self.packet_rate_limit_per_second = 100

    def start(self):
        thread = Thread(target=self.stats_thread)
        thread.start()
        super(L2Switch, self).start()
        print "Start finished"

    def stats_thread(self):
        import socket
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.bind(("192.168.56.1", 9999))
        s.listen(1)

        print "Stats thread started"

        while True:
            client, addr = s.accept()
            data = json.dumps({"number_of_flows": self.number_of_flows,
                               "number_of_packet_ins": self.number_of_packet_ins,
                               "all_packets": self.all_packets})
            client.send(data)
            client.close()

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        """
        Handle switch features reply to install table miss flow entries.
        This function installs table miss flow for both tables.
        """
        datapath = ev.msg.datapath
        [self.install_table_miss(datapath, table) for table in SWITCH_TABLES]
        self.install_broadcast_flow(datapath)

    def create_delete_flow_mod(self, datapath, priority,
                        table_id, match, instructions, action):
        """
        Create OFP flow mod message.
        """
        ofproto = datapath.ofproto

        if action == ofproto.OFPFC_ADD:
            self.number_of_flows[datapath.id] += 1
        elif action == ofproto.OFPFC_DELETE:
            self.number_of_flows[datapath.id] -= 1
        else:
            pass

        flow_mod = datapath.ofproto_parser.OFPFlowMod(datapath, 0, 0, table_id,
                                                      action, 0, 0,
                                                      priority,
                                                      ofproto.OFPCML_NO_BUFFER,
                                                      ofproto.OFPP_ANY,
                                                      OFPG_ANY, 0,
                                                      match, instructions)
        return flow_mod


    def install_broadcast_flow(self, datapath):
        """
        Create and install a flow for flooding all broadcast messages.
        """
        parser = datapath.ofproto_parser
        ofproto = datapath.ofproto
        match = parser.OFPMatch(eth_dst=BROADCAST_MAC)

        flood = parser.OFPActionOutput(ofproto.OFPP_FLOOD)
        write = parser.OFPInstructionActions(ofproto.OFPIT_WRITE_ACTIONS, [flood])
        instructions = [write]
        
        # Create the flow mod itself and send it to the switch
        flow_mod = self.create_delete_flow_mod(datapath, 1, TABLE_ETH_DST,
                                        match, instructions, ofproto.OFPFC_ADD)
        datapath.send_msg(flow_mod)

    def send_set_config(self, datapath):
            ofp = datapath.ofproto
            ofp_parser = datapath.ofproto_parser

            req = ofp_parser.OFPSetConfig(datapath, miss_send_len=0)
            datapath.send_msg(req)

    def install_table_miss(self, datapath, table_id):
        """
        Create and install table miss flow entries.
        """
        parser = datapath.ofproto_parser
        ofproto = datapath.ofproto
        empty_match = parser.OFPMatch()

        # Create an instruction of "send_to_controller" type.
        # We specify NO BUFFER to max_len of the output action due to
        # OVS bug. At this moment, if we specify a lesser number, e.g.,
        # 128, OVS will send Packet-In with invalid buffer_id and
        # truncated packet data. In that case, we cannot output packets
        # correctly.
        output_to_controller = parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                        ofproto.OFPCML_NO_BUFFER)
        write = parser.OFPInstructionActions(ofproto.OFPIT_WRITE_ACTIONS,
                                             [output_to_controller])
        instructions = [write]

        if self.should_install_metering:
            bands = [parser.OFPMeterBandDrop(self.packet_rate_limit_per_second, 0)]
            meter_mod = parser.OFPMeterMod(datapath,
                                           command=ofproto.OFPMC_ADD,
                                           flags=ofproto.OFPMF_PKTPS,
                                           meter_id=table_id,
                                           bands=bands)
            datapath.send_msg(meter_mod)
            instructions.append(parser.OFPInstructionMeter(table_id))


        # Create the flow mod itself and send it to the switch
        flow_mod = self.create_delete_flow_mod(datapath, 0, table_id,
                                        empty_match, instructions, ofproto.OFPFC_ADD)
        datapath.send_msg(flow_mod)

    def get_protocol_names_from_pkt(self, pkt):
        protocol_names = []
        for protocol in pkt.protocols:
            try:
                if protocol.protocol_name not in protocol_names:
                    protocol_names.append(protocol.protocol_name)
            except:
                pass

        return protocol_names

    def parse_packet(self, pkt):
        eth = p_arp = ip = None
        protocol_names = self.get_protocol_names_from_pkt(pkt)

        if "ethernet" in protocol_names:
            eth = pkt.get_protocol(ethernet.ethernet)

        if "arp" in protocol_names:
            p_arp = pkt.get_protocol(arp)

        if "ipv4" in protocol_names:
            ip = pkt.get_protocol(ipv4.ipv4)

        if ip and ethernet:
            return eth.src, eth.dst, ip.src, ip.dst
        elif p_arp and ethernet:
            return p_arp.src_mac, p_arp.dst_mac, p_arp.src_ip, p_arp.dst_ip
        else:
            return None, None, None, None


    def log_packet(self, dpid, src, dst, in_port, msg, src_ip):
        self.logger.info("packet in %s %s %s %s %s %s", dpid, src, dst, hex(in_port), msg.buffer_id, src_ip)


    def is_reactive_arp_reply(self, pkt):
        protocol_names = self.get_protocol_names_from_pkt(pkt)
        if "arp" in protocol_names:
            p_arp = pkt.get_protocol(arp)
        else:
            p_arp = None

        if p_arp and p_arp.opcode == 2 and p_arp.dst_mac == REACTIVE_ARP_FAKE_MAC:
            return True
        else:
            return False

    def is_dhcp_packet(self, pkt):
        protocol_names = self.get_protocol_names_from_pkt(pkt)
        dst_port = None
        if "udp" in protocol_names:
            dst_port = pkt.get_protocol(udp.udp).dst_port

        # Make sure this is not a DHCP packet
        if dst_port != DHCP_PORT:
            return False
        else:
            return True


    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        """Handle packet_in events."""
        msg = ev.msg
        table_id = msg.table_id
        datapath = msg.datapath
        ofproto = datapath.ofproto
        dpid = datapath.id
        in_port = msg.match['in_port']
        pkt = packet.Packet(msg.data)
        data = None
        self.all_packets += 1


        if msg.buffer_id == ofproto.OFP_NO_BUFFER:
            data = msg.data

        src, dst, src_ip, dst_ip = self.parse_packet(pkt)

        if in_port == OFPP_LOCAL or self.is_dhcp_packet(pkt):
            # If we got a packet from a "local" OpenFlow port, we should not currently handle it.
            return

        if src is None or dst is None:
            self.send_packet(datapath, msg.buffer_id, in_port, data, datapath.ofproto.OFPP_FLOOD)
            return

        self.log_packet(dpid, src, dst, in_port, msg, src_ip)
        if self.enable_reactive_arp:
            # Check if the packet is a reply for the reactive ARP query + DROP/Rate-limit
            if self.is_reactive_arp_reply(pkt):
                print "Received Reactive response with (src_mac: %s, src_ip: %s)" % (src, src_ip)
                is_validated = True
                if not src_ip in self.packets_to_verify:
                    return

                for pkt_dict in self.packets_to_verify[src_ip]:
                    original_pkt_in_port = pkt_dict["original_pkt_in_port"]
                    original_pkt_src_mac = pkt_dict["original_pkt_src_mac"]

                    if src == original_pkt_src_mac and in_port == original_pkt_in_port:
                        continue
                    else:
                        is_validated = False
                        break

                if not is_validated:
                    self.lock.acquire()
                    for pkt_dict in self.packets_to_verify[src_ip]:
                        src_mac_to_drop = pkt_dict["original_pkt_src_mac"]
                        if src_mac_to_drop not in self.dropped_macs:
                            print "%s is an Attacker, dropping packets..." % src_mac_to_drop
                            self.install_drop_flow(datapath, src_mac_to_drop)
                            self.dropped_macs.append(src_mac_to_drop)
                    del self.packets_to_verify[src_ip]
                    self.lock.release()
                    return

                # If we are here, the host is validated
                print "Host is validated!"
                self.ip_to_mac[src_ip] = src
                self.rate_limit_physical_port(in_port, dpid)

                self.lock.acquire()
                if len(self.packets_to_verify[src_ip]) > 0:
                    self.number_of_packet_ins += 1
                    self.handle_multiple_validated_packets(self.packets_to_verify[src_ip], src, in_port, datapath)
                    del self.packets_to_verify[src_ip]
                self.lock.release()
                return

            if src_ip not in self.ip_to_mac or src != self.ip_to_mac[src_ip]:
                # Seen before SRC_IP, discrepancy.

                # Don't send multiple ARP queries
                self.lock.acquire()
                if src_ip not in self.packets_to_verify:
                    reactive_arp = self.create_reactive_arp_packet(src_ip, src)

                    # Flood Reactive ARP Packet
                    self.send_packet(datapath, ofproto.OFP_NO_BUFFER, datapath.ofproto.OFPP_CONTROLLER, reactive_arp.data,
                                                  datapath.ofproto.OFPP_FLOOD)
                else:
                    print "src_ip %s is already being validated!" % src_ip

                self.packets_to_verify[src_ip].append({"original_pkt_in_port": in_port, "original_pkt": pkt,
                                           "original_pkt_src_mac": src, "original_msg": msg})
                self.lock.release()
            else:
                # Seen before SRC_IP with same SRC_MAC. Shouldn't be here, unless the dst is unknown
                # TODO: should we send a reactive ARP here too? DST MAC might be spoofed
                assert table_id == TABLE_ETH_DST
                self.number_of_packet_ins += 1

                self.handle_valid_packet(table_id, in_port, src, msg, None)
        else:
            self.number_of_packet_ins += 1
            self.handle_valid_packet(table_id, in_port, src, msg, None)


    def create_reactive_arp_packet(self, src_ip, src_mac):
        e = ethernet.ethernet(
            dst=BROADCAST_MAC,
            src=REACTIVE_ARP_FAKE_MAC,
            ethertype=ether.ETH_TYPE_ARP)
        a = arp(hwtype=1, proto=0x0800, hlen=6, plen=4, opcode=1,
                    src_mac=REACTIVE_ARP_FAKE_MAC, src_ip=REACTIVE_ARP_SRC_IP,
                    dst_mac='00:00:00:00:00:00', dst_ip=src_ip)
        reactive_arp = packet.Packet()
        reactive_arp.add_protocol(e)
        reactive_arp.add_protocol(a)
        reactive_arp.serialize()

        print "Created Reactive ARP Query for src_ip: %s. src_mac for validation: %s" % (src_ip, src_mac)
        return reactive_arp


    def install_drop_flow(self, datapath, src_mac):
        ofp = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch(eth_src=src_mac)

        # Delete old flows for this src_mac
        flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_SRC, match, [], ofp.OFPFC_DELETE)
        datapath.send_msg(flow_mod)

        # Install Drop Flow + Drop packet
        match = parser.OFPMatch(eth_src=src_mac)
        flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_SRC, match, [], ofp.OFPFC_ADD)
        datapath.send_msg(flow_mod)


    def rate_limit_physical_port(self, port, dpid):
        # TODO: make this function work
        if dpid not in self.limited_ports:
            self.limited_ports[dpid] = set([port])
        else:
            self.limited_ports[dpid] |= set([port])
        pass

    def handle_multiple_validated_packets(self, pkts, src_mac, in_port, datapath):
        print "Handling %d packets for host %s" % (len(pkts), src_mac)


        table_id_set = list(set([x["original_msg"].table_id for x in pkts]))
        assert len(table_id_set) == 1 and table_id_set[0] == TABLE_ETH_SRC

        self.install_remove_src_entry(datapath, in_port, src_mac, False)
        self.install_remove_dst_entry(datapath, in_port, src_mac, False)

        for pkt_dict in pkts:
            original_msg = pkt_dict["original_msg"]
            self.send_packet(datapath, original_msg.buffer_id, in_port, None, datapath.ofproto.OFPP_FLOOD)


    def handle_valid_packet(self, table_id, in_port, src, msg, data):
        datapath = msg.datapath
        dpid = datapath.id

        if self.force_flood:
            self.send_packet(datapath, msg.buffer_id, in_port, data, datapath.ofproto.OFPP_FLOOD)
            return

        # Install flow entries
        if table_id == TABLE_ETH_SRC:
            if src in self.mac_to_port[dpid].keys():
                if self.mac_to_port[dpid][src] != in_port:
                    # Src MAC changed its port, remove old flows
                    old_in_port = self.mac_to_port[dpid][src]
                    self.install_remove_src_entry(datapath, old_in_port, src, True)
                    self.install_remove_dst_entry(datapath, old_in_port, src, True)
                else:
                    print "Invalid packet-in! pkt: %s" % packet.Packet(msg.data)
            else:
                # New MAC
                pass
            self.mac_to_port[dpid][src] = in_port

            self.install_remove_src_entry(datapath, in_port, src, False)
            self.install_remove_dst_entry(datapath, in_port, src, False)
        else:
            assert table_id == TABLE_ETH_DST
            # Someone sent a packet to an unknown dst MAC,
            # as we always install src AND dst flows for each new MAC.
            # Getting here means we have already "authenticated" the sender, so we should just rate-limit
            self.rate_limit_physical_port(in_port, dpid)
            pass

        # Flood - only send the data itself back to the switch,
        # if the switch did not notify us of the buffer_id
        self.send_packet(datapath, msg.buffer_id, in_port, data, datapath.ofproto.OFPP_FLOOD)


    def install_remove_src_entry(self, datapath, in_port, eth_src, should_remove):
        """
        Install flow entry matching on (eth_src, in_port) in table 0.
        """
        parser = datapath.ofproto_parser
        ofp = datapath.ofproto
        match = parser.OFPMatch(in_port=in_port, eth_src=eth_src)
                       
        # Create an instruction of "Go to next table" type.
        goto = parser.OFPInstructionGotoTable(TABLE_ETH_DST)

        if not should_remove:
            flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_SRC, match, [goto], ofp.OFPFC_ADD)
        else:
            flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_SRC, match, [], ofp.OFPFC_DELETE)
        datapath.send_msg(flow_mod)


    def install_remove_dst_entry(self, datapath, in_port, eth_src, should_remove):
        """
        Install flow entry matching on (eth_dst) in table 1.
        """
        parser = datapath.ofproto_parser
        ofp = datapath.ofproto
        match = parser.OFPMatch(eth_dst=eth_src)
        
        # Create an instruction of "output to port X" type.
        output_to_correct_port = parser.OFPActionOutput(in_port, ofp.OFPCML_NO_BUFFER)
        write = parser.OFPInstructionActions(ofp.OFPIT_WRITE_ACTIONS,
                                             [output_to_correct_port])

        if not should_remove:
            flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_DST, match, [write], ofp.OFPFC_ADD)
        else:
            flow_mod = self.create_delete_flow_mod(datapath, PRIORITY, TABLE_ETH_DST, match, [], ofp.OFPFC_DELETE)
        datapath.send_msg(flow_mod)


    def send_packet(self, datapath, buffer_id, in_port, data, out_port):
        """
        Send a packet_out with output to all ports.
        """
        parser = datapath.ofproto_parser
        ofp = datapath.ofproto
        
        # Send a "packet_out" packet back to the switch, in order to flood the packet.
        output__to_all_ports = parser.OFPActionOutput(out_port)
        packet_out = parser.OFPPacketOut(datapath=datapath,
                                         buffer_id=buffer_id,
                                         in_port=in_port,
                                         actions=[output__to_all_ports],
                                         data=data)
        datapath.send_msg(packet_out)